import React, { Component, useState } from 'react';
import { Image, View, Pressable, Dimensions, Text, StyleSheet, ImageBackground, SafeAreaView, TextInput, TouchableOpacity} from 'react-native';


const images = [
    {
        id: "0",
        name: "Shoes",
    },
    {
        id: "1",
        name: "Books",
    },
    {
        id: "2",
        name: "Novels",
    },
    {
        id: "3",
        name: "Uniform",
    },
    {
        id: "4",
        name: "Stars",
    },
];

const AddItem = () => {
    const [plane,setPlane] = useState([]);
    console.log("plane items",plane)
    return (
        <>
        <Text style={{ textAlign: "center", fontSize: 20}}>
            List of available items
        </Text>
        {images.map((item) => (
            <Pressable
            key={item.id}
            style={{ flexDirection: "row", alignItems: "center" }}
            >
                <View style={{ margin: 1}}>
                    <Image 
                        style={{ width: 10,
                        height: 10, borderRadius: 8, backgroundColor:'red'}}
                        source={{ uri: item.name }}
                        />
                        </View>
                        <View>
                            <text style={{ fontWeight: "bold" }}>{item.name}</text>
                            {plane.includes(item) ? (
                            <Pressable onPress={() => setPlane(plane.filter((x) => x.id !== item.id))}>
                            <Text
                                 style={{
                                    borderColor: "grey",
                                    borderWidth: 1,
                                    marginVertical: 10,
                                    padding: 5,
                                    }}
                                 >
                                     REMOVE FROM PLANE
                                        </Text>
                                    </Pressable>
                            ):(
                                <Pressable onPress={() => setPlane([...plane,item])} >
                                <Text
                                style={{
                                    borderColor: "grey",
                                    borderWidth: 1,
                                    marginVertical: 10,
                                    padding: 5,
                                }}
                                >
                                    ADD TO NEW PLANE
                                </Text>
                            </Pressable> 
                            )}
                            
                            </View>
                            </Pressable>
        
        )
        )
                            }
                            <View style={{ height: 10, borderColor: "grey", borderWidth: 10}} />
                            <Text style={{fontSize: 30,}}>Added items:</Text>
                            {plane.map((item) => (
                                <View>
                                     <Image 
                        style={{ width: 10,
                        height: 10, borderRadius: 8, }}
                        source={{ uri: item.name }}
                        />
                        <Text style={{fontSize: 20, color: 'lime'}}>{item.name}</Text>
                                </View>
                            ))}
        </>
    );
};

export default AddItem;

const styles = StyleSheet.create({});