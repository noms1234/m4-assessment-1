import React, { useState, useEffect } from 'react';
import { Platform, Text, View, StyleSheet, Dimensions } from 'react-native';
import * as Location from 'expo-location';
import MapView from 'react-native-maps';
	

const MapServ = ({ navigation }) => {
    const [currentLocaton, setCurrentLocation] = useState(null);

    useEffect(() => {
        (async () => {
          let { status } = await Location.requestForegroundPermissionsAsync();
          if (status !== 'granted') {
            setErrorMsg('Permission to access location was denied');
            return;
          }
    
          let location = await Location.getCurrentPositionAsync({});
          setLocation(location);
        })();
      }, []);
    
      return (
        <View style={styles.container}>
          <MapView style={styles.map}
          initialRegion={{
            latitude: -29.858681,
            longitude: 31.021839,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
          />
        </View>
      );
    }
    

// syle propeties
const styles = StyleSheet.create({
    container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
    },
    map: {
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
      },
});

export default MapServ;

